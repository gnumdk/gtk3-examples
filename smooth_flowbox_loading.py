"""
	How to load 2000 images with a FlowBox and without any lag
"""

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import GLib, Gtk, Gdk, GObject, GdkPixbuf

from threading import Thread

class ImageWidget(Gtk.Image):
    __gsignals__ = {
        "populated": (GObject.SignalFlags.RUN_FIRST, None, ()),
    }
    def __init__(self):
        Gtk.Image.__init__(self)
    
    def load(self, path):
        """
            Mainloop
        """
        thread = Thread(target=self.load_pixbuf,
                        args=(path,))
        thread.daemon = True
        thread.start()

    def load_pixbuf(self, path):
        """
            Thread
        """
        pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_size(path, 100, 100)
        GLib.idle_add(self.load_surface, pixbuf)
        
    def load_surface(self, pixbuf):
        """
            Mainloop
        """
        surface = Gdk.cairo_surface_create_from_pixbuf(
                    pixbuf, self.get_scale_factor(), None)
        self.set_from_surface(surface)
        self.emit("populated")
        
        

class MyDemoApp():

    image = "/usr/share/backgrounds/gnome/adwaita-day.jpg"

    def __init__(self):
        window = Gtk.Window()
        window.set_title("My demo window")
        window.set_default_size(500, 300)
        window.set_position(Gtk.WindowPosition.CENTER)
        window.connect('destroy', self.destroy)
        scrolled = Gtk.ScrolledWindow()
        scrolled.set_property("expand", True)
        self.flowbox = Gtk.FlowBox()
        self.flowbox.set_hexpand(True)
        self.flowbox.set_property("valign", Gtk.Align.START)
        self.flowbox.set_homogeneous(True)
        scrolled.add(self.flowbox)
        window.add(scrolled)
        window.show_all()
        
    def load(self, count):
        images = []
        while count > 0:
            images.append(self.image)
            count -= 1
        self.load_images(images)

    def load_images(self, images):
        if images:
            image = images.pop(0)
            image_widget = ImageWidget()
            image_widget.show()
            image_widget.connect("populated", self.on_populated, images)
            image_widget.load(image)
            self.flowbox.add(image_widget)

    def on_populated(self, image_widget, images):
        self.load_images(images)

    def destroy(self, window):
        Gtk.main_quit()

def main():
    app = MyDemoApp()
    app.load(2000)
    Gtk.main()


if __name__ == '__main__':
    main()
